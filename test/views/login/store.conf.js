// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

import app from '@/store/modules/app'
import errorLog from '@/store/modules/errorLog'
import user from '@/store/modules/user'
import users from '@/store/modules/users'
import getters from '@/store/getters'

export default {
  modules: {
    app,
    errorLog,
    user,
    users
  },
  getters
}

