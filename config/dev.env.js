// SPDX-FileCopyrightText: 2017-2019 PanJiaChen <https://github.com/PanJiaChen/vue-element-admin>
// SPDX-License-Identifier: MIT
//
// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

module.exports = {
  NODE_ENV: '"development"',
  ENV_CONFIG: '"dev"',
  DISABLED_FEATURES: '[""]',
  ASSETS_PUBLIC_PATH: '/'
}
