// SPDX-FileCopyrightText: 2017-2019 PanJiaChen <https://github.com/PanJiaChen/vue-element-admin>
// SPDX-License-Identifier: MIT

module.exports = {
  NODE_ENV: '"production"',
  ENV_CONFIG: '"sit"',
  BASE_API: '"https://api-sit"'
}
