// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

export async function needReboot(authHost, token) {
  return Promise.resolve({ data: false })
}

export async function restartApp(authHost, token) {
  return Promise.resolve()
}
