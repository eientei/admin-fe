// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

const isLocalhost = (instanceName) =>
  instanceName.startsWith('localhost:') || instanceName.startsWith('127.0.0.1:')

export const baseName = (instanceName = 'localhost') => {
  if (instanceName.match(/https?:\/\//)) {
    return instanceName
  } else {
    return isLocalhost(instanceName) ? `http://${instanceName}` : `https://${instanceName}`
  }
}
