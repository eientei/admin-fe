// SPDX-FileCopyrightText: 2017-2019 PanJiaChen <https://github.com/PanJiaChen/vue-element-admin>
// SPDX-License-Identifier: MIT
//
// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

import store from '@/store'

const { body } = document
const mobileWidth = 480
const tabletWidth = 801
const ratio = 3

export default {
  watch: {
    $route(route) {
      if (this.device === 'mobile' && this.sidebar.opened) {
        store.dispatch('closeSideBar', { withoutAnimation: false })
      }
    }
  },
  beforeMount() {
    window.addEventListener('resize', this.resizeHandler)
  },
  mounted() {
    const isMobile = this.isMobile()
    const isTablet = this.isTablet()
    if (isMobile || isTablet) {
      store.dispatch('toggleDevice', isMobile ? 'mobile' : 'tablet')
      store.dispatch('closeSideBar', { withoutAnimation: true })
    }
  },
  methods: {
    isMobile() {
      const rect = body.getBoundingClientRect()
      return rect.width - ratio < mobileWidth
    },
    isTablet() {
      const rect = body.getBoundingClientRect()
      return rect.width - ratio < tabletWidth && rect.width - ratio > mobileWidth
    },
    resizeHandler() {
      if (!document.hidden) {
        const isMobile = this.isMobile()
        const isTablet = this.isTablet()

        if (isMobile || isTablet) {
          store.dispatch('toggleDevice', isMobile ? 'mobile' : 'tablet')
          store.dispatch('closeSideBar', { withoutAnimation: true })
        } else {
          store.dispatch('toggleDevice', 'desktop')
        }
      }
    }
  }
}
