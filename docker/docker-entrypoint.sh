#!/bin/sh

# SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
# SPDX-License-Identifier: AGPL-3.0-only

set -e
envsubst "$(env | sed -e 's/=.*//' -e 's/^/\$/g')" < "/etc/nginx/nginx.conf.tpl" > "/etc/nginx/nginx.conf"
exec "$@"
